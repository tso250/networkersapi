package springboot.networkersAPI.utils;

public enum eRequestMethod{
	get, post, put
}