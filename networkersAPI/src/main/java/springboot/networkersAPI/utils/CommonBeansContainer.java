package springboot.networkersAPI.utils;

import javax.annotation.PostConstruct;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class CommonBeansContainer {

	private ModelMapper modelMapper;
	
	public CommonBeansContainer() {
	}
	
	@PostConstruct
	private void postConstruct() {
		modelMapper = new ModelMapper();
	}

	public ModelMapper getModelMapper() {
		return modelMapper;
	}
	
}
