package springboot.networkersAPI.utils;

import org.springframework.http.ResponseEntity;

public class LoggingUtils {

	public static String DisplayHTTPResponseData(ResponseEntity res) {
		String data = String.format("HTTP status code: %s , Headers: %s , Body: %s",res.getStatusCode().value(),res.getHeaders(),res.getBody());
		return data;
	}
}
