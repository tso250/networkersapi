package springboot.networkersAPI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private DataSource dataSource;

    @Value("${spring.queries.users-query}")
    private String usersQuery;

    @Value("${spring.queries.roles-query}")
    private String rolesQuery;

    @Override
    protected void configure(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.
                jdbcAuthentication()
                .usersByUsernameQuery(usersQuery)
                .authoritiesByUsernameQuery(rolesQuery)
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception { //TODO : /api/getChangedUsersData is temp with permitAll - need to change

        http.cors().and().
                authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers(HttpMethod.GET, "/api/getChangedUsersData").permitAll()
                .antMatchers(HttpMethod.POST, "/api/login").permitAll()
                .antMatchers(HttpMethod.POST, "/api/isEmailUnique").permitAll()
                .antMatchers(HttpMethod.GET, "/api/lookups").permitAll()
                .antMatchers(HttpMethod.POST, "/api/signup").permitAll()
                .antMatchers(HttpMethod.POST, "/api/search").permitAll()
                .antMatchers("/AboutUs").permitAll()
                .antMatchers("/TermService").permitAll()
                .antMatchers("/OurVision").permitAll()
                .antMatchers("/SignUp").permitAll()
                .antMatchers("/SignIn").permitAll()
                .antMatchers("/DashboardPage").permitAll()
                .antMatchers(HttpMethod.GET, "/api/logout").hasAuthority("USER")
                .antMatchers(HttpMethod.POST, "/api/getProfile").hasAuthority("USER")
                .antMatchers(HttpMethod.POST, "/api/updateProfile").hasAuthority("USER")
                .antMatchers(HttpMethod.POST, "/api/contactMe/{\\d+}").hasAuthority("USER")
                .antMatchers(HttpMethod.GET, "/api/contactMe").hasAuthority("USER")
                .antMatchers("/EditProfile").hasAuthority("USER")
                .antMatchers("/Profile").hasAuthority("USER")
                .anyRequest()
                .authenticated().and().csrf().disable().formLogin()
                .loginPage("/SignIn").failureUrl("/NotFoundPage")
                .defaultSuccessUrl("/Profile")
                .usernameParameter("email")
                .passwordParameter("password")
                .and().logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/Logout"))
                .logoutSuccessUrl("/DashboardPage").and().exceptionHandling()
                .accessDeniedPage("/NotFoundPage");
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/resources/**", "/static/**", "/built/**", "/css/**", "/js/**", "/images/**");
    }

    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}
