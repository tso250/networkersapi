package springboot.networkersAPI.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import springboot.networkersAPI.model.Role;

@Service
@Transactional
public class RoleServiceImpl extends RoleService{
	
	@Override
	public Role getRole(int id) {
        return roleDAOImpl.getRole(id);
	}

	@Override
	public void initRoles() {
		roleDAOImpl.initRoles();
	}

}
