package springboot.networkersAPI.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;

import springboot.networkersAPI.dao.ContactMeMessageDAOImpl;
import springboot.networkersAPI.model.ContactMeMessage;
import springboot.networkersAPI.model.User;


public abstract class ContactMeMessageService {

	@Autowired
	@Qualifier("contactMeMessageDAOImpl")
	protected ContactMeMessageDAOImpl contactMeMessageDAOImpl;
	
	public abstract ResponseEntity addMessage(ContactMeMessage message);

	public abstract List<ContactMeMessage> getUserContactMeMessages(User user);

	public abstract ContactMeMessage getContactMeMessage(Long messageID);

	public abstract ContactMeMessage updateContactMeMessage(ContactMeMessage contactMeMessageToDelete);
}
