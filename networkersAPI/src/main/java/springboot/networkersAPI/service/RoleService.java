package springboot.networkersAPI.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import springboot.networkersAPI.dao.RoleDAO;
import springboot.networkersAPI.model.Role;

public abstract class RoleService {
	
	@Autowired
	@Qualifier("roleDAOImpl")
	protected RoleDAO roleDAOImpl;
	
    public abstract Role getRole(int id);
    
    public abstract void initRoles();

}
