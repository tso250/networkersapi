package springboot.networkersAPI.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.querydsl.core.BooleanBuilder;

import springboot.networkersAPI.dao.RoleDAO;
import springboot.networkersAPI.dao.UserDAO;
import springboot.networkersAPI.model.Lookup;
import springboot.networkersAPI.model.User;

public abstract class UserService {
	
	@Autowired
	@Qualifier("userDAOImpl")
	protected UserDAO userDAOImpl;
	
	@Autowired
	@Qualifier("roleDAOImpl")
	protected RoleDAO roleDAOImpl;
	
    public abstract User getUser(String login);

	public abstract ResponseEntity addUser(User user) throws Exception;
	
	public abstract ResponseEntity isEmailUnique(String email);

	public abstract Lookup getLookups();

	public abstract HttpStatus updateUser(User user);
	
	public abstract boolean doesUserExists(Integer id);

	public abstract List<User> findByWasDataUpdated(boolean b);

	public abstract void updateWasDataUpdatedField(List<Integer> idsOfUpdatedUsers, boolean b);

	public abstract Iterable<User> findAll(BooleanBuilder queryBuilder);
}
