package springboot.networkersAPI.service;

import java.util.List;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.BooleanExpression;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import springboot.networkersAPI.model.ContactMeMessage;
import springboot.networkersAPI.model.QContactMeMessage;
import springboot.networkersAPI.model.User;

@Service
@Transactional
public class ContactMeMessageServiceImpl extends ContactMeMessageService{

	@Override
	public ResponseEntity addMessage(ContactMeMessage message) {
		return contactMeMessageDAOImpl.addMessage(message);
	}

	@Override
	public List<ContactMeMessage> getUserContactMeMessages(User user){
		QContactMeMessage qContactMeMessage = QContactMeMessage.contactMeMessage;
		BooleanBuilder builder = new BooleanBuilder();

		BooleanExpression isContactMeMessageSentToUserID = qContactMeMessage.recipientID.eq(user.getId());
		builder.and(isContactMeMessageSentToUserID);

		// search only for active users
		BooleanExpression isContactMeMessageNotDeleted = qContactMeMessage.isDeletedByRecipient.eq(false);
		builder.and(isContactMeMessageNotDeleted);

		return contactMeMessageDAOImpl.getUserContactMeMessages(builder);
	}

	@Override
	public ContactMeMessage getContactMeMessage(Long messageID) {
		return contactMeMessageDAOImpl.getContactMeMessageByID(messageID);
	}

	@Override
	public ContactMeMessage updateContactMeMessage(ContactMeMessage contactMeMessageToUpdate) {
		return contactMeMessageDAOImpl.updateContactMeMessage(contactMeMessageToUpdate);
	}
}
