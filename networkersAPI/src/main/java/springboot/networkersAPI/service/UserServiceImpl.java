package springboot.networkersAPI.service;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;

import springboot.networkersAPI.model.Lookup;
import springboot.networkersAPI.model.User;

@Service
@Transactional
public class UserServiceImpl extends UserService{
	
	private static final Lookup s_lookup = new Lookup();
    public UserServiceImpl() {
    	super();
	}
 
	@Override
	public User getUser(String login) {
        return userDAOImpl.getUser(login);

	}

	@Override
	public ResponseEntity addUser(User user) throws Exception{
		return userDAOImpl.addUser(user);
	}

	@Override
	public ResponseEntity isEmailUnique(String email) {
		return userDAOImpl.isEmailUnique(email);
	}

	@Override
	public HttpStatus updateUser(User user){
		return userDAOImpl.updateUser(user);
	}

	public boolean doesUserExists(Integer id) {
		return userDAOImpl.doesUserExists(id);
	}

	@Override
	public Iterable<User> findAll(BooleanBuilder queryBuilder) {
		return userDAOImpl.findAll(queryBuilder);
	}
	
	@Override
	public List<User> findByWasDataUpdated(boolean b) {
		return userDAOImpl.findByWasDataUpdated(b);
	}

	@Override
	public void updateWasDataUpdatedField(List<Integer> idsOfUpdatedUsers, boolean b) {
		userDAOImpl.updateWasDataUpdatedField(idsOfUpdatedUsers, b);
		
	}

	@Override
	public Lookup getLookups() {
		return s_lookup;
	}
}
