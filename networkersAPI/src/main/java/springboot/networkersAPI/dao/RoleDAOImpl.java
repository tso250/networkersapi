package springboot.networkersAPI.dao;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import springboot.networkersAPI.model.Role;


@Repository
public class RoleDAOImpl extends RoleDAO{
    

	@Autowired
	private RoleRepo roleRepo;
	
	public Role getRole(int id) {
		try {
        Optional<Role> role = roleRepo.findById(new Long(id));
        return role.get();
        }
		catch(NoSuchElementException e){
			e.printStackTrace();
			return null;
		}
	}
	
	public Role getRoleByName(String roleName) {
		try {
			Optional<Role> role =roleRepo.findByrole(roleName);
			return role.get();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public void initRoles() {
		try {
			Role role = getRoleByName("USER");
			if (role == null) {
				role = new Role();
				role.setRole("USER");
				roleRepo.save(role);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
