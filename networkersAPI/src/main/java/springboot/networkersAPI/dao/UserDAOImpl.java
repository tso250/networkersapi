package springboot.networkersAPI.dao;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import com.querydsl.core.BooleanBuilder;

import springboot.networkersAPI.model.Role;
import springboot.networkersAPI.model.User;

@Repository
public class UserDAOImpl extends UserDAO {

	@Autowired
	private UserRepo userRepo;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	@Qualifier("roleDAOImpl")
	protected RoleDAO roleDAOImpl;
	
	public UserDAOImpl() {
		super();
	}

	public User getUser(String email) {
		
		try {
			Optional<User> user = userRepo.findByemail(email);
			return user.get();
		}
		catch(NoSuchElementException e) {
			e.printStackTrace();
			return null;
		}
	}

	public ResponseEntity<String> addUser(User user) throws Exception{		
		//Response response = new Response();
		HttpStatus responseStatusCode;
		String bodyErrorMessage = null;
		if (getUser(user.getEmail()) == null) {
				user.setPassword(passwordEncoder.encode(user.getPassword()));
				user.setActive(1);
				user.setWasDataUpdated(true);

				Role role = roleDAOImpl.getRoleByName("USER");
				
				user.setRoles(new HashSet<Role>(Arrays.asList(role)));

				userRepo.save(user);

				//response.setIsStatusOK(true);
				responseStatusCode = HttpStatus.OK;
		} else {
			//response.setMessage("User email already exists!");
			responseStatusCode = HttpStatus.CONFLICT;
			bodyErrorMessage = "User email already exists!";
		}
		ResponseEntity<String> response;
		if(responseStatusCode == HttpStatus.OK) {
			response = new ResponseEntity<String>(HttpStatus.OK);
		}
		else {
			response = ResponseEntity.status(responseStatusCode).body(bodyErrorMessage);
		}
		return response;

	}

	public HttpStatus updateUser(User user) {

		HttpStatus responseStatusCode;
		//Response response = new Response();

		try {			
			userRepo.save(user);
			responseStatusCode = HttpStatus.OK;
			//response.setIsStatusOK(true);
		} catch (Exception e) {
			e.printStackTrace();
			responseStatusCode = HttpStatus.INTERNAL_SERVER_ERROR;
			//response.setMessage(e.toString());
		}
		
		return responseStatusCode;

	}

	@Override
	public ResponseEntity isEmailUnique(String email) {
		//Response response = new Response();
		HttpStatus responseStatusCode;
		
		if (getUser(email) == null) {
			responseStatusCode = HttpStatus.OK;
			//response.setIsStatusOK(true);
		} else {
			responseStatusCode = HttpStatus.CONFLICT;
			//response.setMessage("User email already exists!");
		}
		ResponseEntity<String> response;
		if(responseStatusCode == HttpStatus.OK) {
			response = new ResponseEntity<String>(HttpStatus.OK);
		}
		else {
			response = ResponseEntity.status(responseStatusCode).body("User email already exists!");
		}
		
		return response;
	}

	@Override
	public boolean doesUserExists(Integer id) {
		Optional<User> user = userRepo.findById(id);
		if(user.isPresent() && user.get()!=null)
			return true;
		return false;
	}



	@Override
	public Iterable<User> findAll(BooleanBuilder queryBuilder) {
		return userRepo.findAll(queryBuilder);
	}
	
	@Override
	public List<User> findByWasDataUpdated(boolean b) {
		return userRepo.findByWasDataUpdated(b);
	}

	@Override
	public void updateWasDataUpdatedField(List<Integer> idsOfUpdatedUsers, boolean b) {
		userRepo.updateWasDataUpdatedField(idsOfUpdatedUsers, b);
		
	}
	
}
