package springboot.networkersAPI.dao;

import java.util.List;

import com.querydsl.core.BooleanBuilder;

import org.springframework.http.ResponseEntity;

import springboot.networkersAPI.model.ContactMeMessage;


public abstract class ContactMeMessageDAO {
	
	public abstract ResponseEntity addMessage(ContactMeMessage message);
	public abstract List<ContactMeMessage> getUserContactMeMessages(BooleanBuilder builder);
	public abstract ContactMeMessage getContactMeMessageByID(Long messageID);
	public abstract ContactMeMessage updateContactMeMessage(ContactMeMessage message);
}
