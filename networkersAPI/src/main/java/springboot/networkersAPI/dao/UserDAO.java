package springboot.networkersAPI.dao;

import org.springframework.http.ResponseEntity;

import com.querydsl.core.BooleanBuilder;

import java.util.List;

import org.springframework.http.HttpStatus;

import springboot.networkersAPI.model.User;


public abstract class UserDAO {
	
	
    public abstract User getUser(String login);

	public abstract ResponseEntity addUser(User user) throws Exception;

	public abstract ResponseEntity isEmailUnique(String email);

	public abstract HttpStatus updateUser(User user);
	
	public abstract boolean doesUserExists(Integer id);

	public abstract Iterable<User> findAll(BooleanBuilder queryBuilder);
	
	public abstract List<User> findByWasDataUpdated(boolean b);

	public abstract void updateWasDataUpdatedField(List<Integer> idsOfUpdatedUsers, boolean b);

}
