package springboot.networkersAPI.dao;

import springboot.networkersAPI.model.Role;

public abstract class RoleDAO {
	
    public abstract Role getRole(int id);
    
    public abstract Role getRoleByName(String roleName);
    
    public abstract void initRoles();

}
