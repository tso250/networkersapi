package springboot.networkersAPI.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import springboot.networkersAPI.model.ContactMeMessage;

@Repository
public interface ContactMeMessageRepo extends CrudRepository<ContactMeMessage, Long>, QuerydslPredicateExecutor<ContactMeMessage> {

	List<ContactMeMessage> findAll();
	//List<ContactMeMessage> findByArea(eArea area);
	//Optional<ContactMeMessage> findByemail(String email);
}
