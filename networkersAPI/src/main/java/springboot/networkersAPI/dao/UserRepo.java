package springboot.networkersAPI.dao;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import springboot.networkersAPI.model.User;
import springboot.networkersAPI.model.eArea;

@Repository
public interface UserRepo extends CrudRepository<User, Integer>, QuerydslPredicateExecutor<User> {

	List<User> findAll();
	List<User> findByArea(eArea area);
	Optional<User> findByemail(String email);
	//Optional<User> findByuser_id(Integer id);
	List<User> findByWasDataUpdated(Boolean wasDataUpdated);
	
	//UPDATE user u set u.was_data_updated = 0 WHERE u.user_id IN (1,2,3)
	@Modifying
	@Query(value = "UPDATE User u SET u.wasDataUpdated = :val WHERE u.id IN :ids")
	void updateWasDataUpdatedField(@Param("ids") Collection<Integer> ids, @Param("val") Boolean value);
}
