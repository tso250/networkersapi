package springboot.networkersAPI.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.querydsl.core.BooleanBuilder;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import springboot.networkersAPI.model.ContactMeMessage;

@Repository
public class ContactMeMessageDAOImpl extends ContactMeMessageDAO {

	private final static Logger LOG = Logger.getLogger(ContactMeMessageDAOImpl.class);
	
	@Autowired
	private ContactMeMessageRepo contactMeMessageRepo;
	
	public ContactMeMessageDAOImpl() {
		super();
	}

	@Override
	public ResponseEntity addMessage(ContactMeMessage message) {
		HttpStatus responseStatusCode;
		try {
			contactMeMessageRepo.save(message);
			responseStatusCode = HttpStatus.OK;
		}
		catch(Exception e) {
			e.printStackTrace();
			responseStatusCode = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return new ResponseEntity(responseStatusCode);

	}

	@Override
	public List<ContactMeMessage> getUserContactMeMessages(BooleanBuilder builder){
		List<ContactMeMessage> allMessages = new ArrayList<ContactMeMessage>();
		try {
			for (ContactMeMessage message : contactMeMessageRepo.findAll(builder)) {
				allMessages.add(message);
			}
		}
		catch(Exception e) {
			LOG.error(String.format("Error::getUserContactMeMessages::Exception::%s", e.getCause()));
			e.printStackTrace();
		}
		return allMessages;
	}

	@Override
	public ContactMeMessage getContactMeMessageByID(Long messageID) {
		try {
			Optional<ContactMeMessage> contactMeMessage = contactMeMessageRepo.findById(messageID);
			if(contactMeMessage.isPresent())
				return contactMeMessage.get();
		}
		catch(Exception e) {
			LOG.error(String.format("Error::getUserContactMeMessages::Exception::%s", e.getCause()));
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ContactMeMessage updateContactMeMessage(ContactMeMessage message) {
		try {
			return contactMeMessageRepo.save(message);
		}
		catch(Exception e) {
			LOG.error(String.format("Error::updateContactMeMessage::Exception::%s", e.getCause()));
		}
		return null;
	}
}
