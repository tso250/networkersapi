package springboot.networkersAPI;


import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class App 
{
	
	
    private final static Logger LOG = Logger.getLogger(App.class);
    
    public static void main( String[] args )
    {
    	LOG.info("Start::App::LOG::main");
    	SpringApplication.run(App.class, args);
     	LOG.info("Started::App::LOG::main");
    }
    
}
