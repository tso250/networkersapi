package springboot.networkersAPI.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {

    private final static Logger LOG = Logger.getLogger(HomeController.class);
	@RequestMapping(value = {"/", "/error", "/{path:[^\\.]*}", "/**/{path:^(?!oauth).*}/{path:[^\\.]*}"}, method = RequestMethod.GET)
	public String index() {

		LOG.info("HomeController::index");
		return "index.html";
	}

}
