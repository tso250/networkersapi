package springboot.networkersAPI.controller;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import springboot.networkersAPI.model.Lookup;
import springboot.networkersAPI.model.User;
import springboot.networkersAPI.service.UserService;
import springboot.networkersAPI.utils.LoggingUtils;

@RestController
public class SignupController {

	private final static Logger LOG = Logger.getLogger(SignupController.class);

	@Autowired
	private UserService userService;

	@Autowired
	AuthenticationManager authenticationManager;

	public SignupController() {
	}

	@PostMapping(value = "api/signup", produces = "application/json")
	public ResponseEntity signup(@Valid @RequestBody User user) {
		ResponseEntity res;
		LOG.info(String.format("Start::SignupController::signup::user[%s]", user));
		try {		
			res = userService.addUser(user);		
		}
		catch(Exception e) {
			res = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
			LOG.error(String.format("Error::SignupController::signup::Exception::%s", e.getCause()));
			e.printStackTrace();
		}
		LOG.info(String.format("End::SignupController::signup::user[%s]::res[%s]", user, LoggingUtils.DisplayHTTPResponseData(res)));
		return res;
	}

	@PostMapping(value = "api/isEmailUnique", produces = "application/json")
	public ResponseEntity<String> isUserExists(@RequestBody String email) {
		ResponseEntity res;
		LOG.info(String.format("Start::SignupController::isUserExists::user[%s]", email));
		try {
			res = userService.isEmailUnique(email);
		}
		catch(Exception e) {
			res = new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
			LOG.error(String.format("Error::SignupController::isUserExists::Exception::%s", e.getCause()));
			e.printStackTrace();
		}
		LOG.info(String.format("End::SignupController::isUserExists::user[%s]::res[%s]", email, LoggingUtils.DisplayHTTPResponseData(res)));
		return res;
	}

	@GetMapping(value = "api/lookups", produces = "application/json")
	public ResponseEntity<Lookup> getLookups() {
		ResponseEntity<Lookup> res;
		LOG.info(String.format("Start::SignupController::getLookups"));
		try {
			Lookup lookup = userService.getLookups();
			res = new ResponseEntity<Lookup>(lookup, HttpStatus.OK);
		}
		catch(Exception e) {
			res = new ResponseEntity<Lookup>(HttpStatus.INTERNAL_SERVER_ERROR);
			LOG.error(String.format("Error::SignupController::getLookups::Exception::%s", e.getCause()));
			e.printStackTrace();
		}
		LOG.info(String.format("End::SignupController::getLookups::res[%s]", LoggingUtils.DisplayHTTPResponseData(res)));
		return res;
	}

}
