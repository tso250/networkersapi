package springboot.networkersAPI.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import springboot.networkersAPI.model.ContactMeMessage;
import springboot.networkersAPI.model.SentMessage;
import springboot.networkersAPI.model.User;
import springboot.networkersAPI.service.ContactMeMessageService;
import springboot.networkersAPI.service.UserService;
import springboot.networkersAPI.utils.LoggingUtils;

@RestController
public class ContactMeController {

	private final static Logger LOG = Logger.getLogger(ContactMeController.class);

	@Autowired
	private ContactMeMessageService contactMeMessageService;

	@Autowired
	private UserService userService;

	@PostMapping(value = "api/contactMe/{recipientID}", produces = "application/json")
	public ResponseEntity sendContactMeMessage(@Valid @RequestBody SentMessage sentMessage,
			@PathVariable int recipientID) {
		HttpStatus responseStatusCode;
		LOG.info(String.format("Start::ContactMeController::sendMessage"));
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		LOG.info(String.format("ContactMeController::sendMessage::auth[%s]::isAuthenticated[%s]", auth.getName(),
				String.valueOf(auth.isAuthenticated())));
		if (auth != null && auth.isAuthenticated()) {
			boolean doesRecipientExists = userService.doesUserExists(recipientID);
			if (doesRecipientExists) {
				User user = userService.getUser(auth.getName());
				if (user != null) {
					int senderID = user.getId();
					String firstName = user.getFirstName();
					String lastName = user.getLastName();
					String senderDisplayName = firstName + " " + lastName;
					Calendar timeStamp = Calendar.getInstance();

					ContactMeMessage message = new ContactMeMessage(senderID, senderDisplayName, recipientID,
							sentMessage.getMessageBody(), timeStamp, sentMessage.getTitle());
							
					contactMeMessageService.addMessage(message);
					responseStatusCode = HttpStatus.OK;
				} else {
					responseStatusCode = HttpStatus.NOT_FOUND;
				}
			} else {
				responseStatusCode = HttpStatus.NOT_FOUND;
			}
		} else {
			responseStatusCode = HttpStatus.UNAUTHORIZED;
		}

		return new ResponseEntity(responseStatusCode);
	}

	@PutMapping(value = "api/contactMe/{messageID}", produces = "application/json")
	public ResponseEntity updateContactMeMessage(@Valid @RequestBody ContactMeMessage contactMeMessageToUpdate,
			@PathVariable Long messageID) {
		HttpStatus responseStatusCode;
		LOG.info(String.format("Start::ContactMeController::updateContactMeMessage::requestMessageID[%d]", messageID));
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		LOG.info(String.format("ContactMeController::updateContactMeMessage::auth[%s]::isAuthenticated[%s]", auth.getName(),
				String.valueOf(auth.isAuthenticated())));
		if (auth != null && auth.isAuthenticated()) {
			User recipientUser = userService.getUser(auth.getName());
			if (recipientUser != null) {
				ContactMeMessage contactMeMessage = contactMeMessageService.getContactMeMessage(messageID);
				if (contactMeMessage != null) {
					int recipientUserID = recipientUser.getId();
					if (contactMeMessage.getRecipientID() == recipientUserID) {
						if(contactMeMessageToUpdate.getIsReadByRecipient()){
							contactMeMessage.setIsReadByRecipient(contactMeMessageToUpdate.getIsReadByRecipient());
							ContactMeMessage updatedMessage = contactMeMessageService.updateContactMeMessage(contactMeMessage);
							LOG.info(String.format("ContactMeController::updateContactMeMessage::Success::Updated message succesfully requestMessage[%s]", updatedMessage.toString()));
							responseStatusCode = HttpStatus.OK;
						}
						else{
							LOG.error("ContactMeController::updateContactMeMessage::Bad Request::Have to set isReadByRecipient to true");
							responseStatusCode = HttpStatus.BAD_REQUEST;	
						}
					} else {
						LOG.error("ContactMeController::updateContactMeMessage::Security Error::Tried to update message of another recipient.");
						responseStatusCode = HttpStatus.UNAUTHORIZED;
					}
				} else {
					LOG.warn("ContactMeController::updateContactMeMessage::Not Found::Tried to update a non existing Contact Me Message.");
					responseStatusCode = HttpStatus.NOT_FOUND;
				}
			} else {
				LOG.error("ContactMeController::updateContactMeMessage::Security Error::Tried to update message with non existing user session.");
				responseStatusCode = HttpStatus.UNAUTHORIZED;
			}
		} else {
			LOG.info("ContactMeController::updateContactMeMessage::Security Error::Tried to update message without authenticated token.");
			responseStatusCode = HttpStatus.UNAUTHORIZED;
		}

		return new ResponseEntity(responseStatusCode);
	}

	@DeleteMapping(value = "api/contactMe/{messageID}", produces = "application/json")
	public ResponseEntity deleteContactMeMessageForRecipient(@PathVariable Long messageID) {
		HttpStatus responseStatusCode;
		LOG.info(String.format("Start::ContactMeController::deleteContactMeMessageForRecipient::requestMessageID[%d]", messageID));
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		LOG.info(String.format("ContactMeController::deleteContactMeMessageForRecipient::auth[%s]::isAuthenticated[%s]", auth.getName(),
				String.valueOf(auth.isAuthenticated())));
		if (auth != null && auth.isAuthenticated()) {
			User recipientUser = userService.getUser(auth.getName());
			if (recipientUser != null) {
				ContactMeMessage contactMeMessageToDelete = contactMeMessageService.getContactMeMessage(messageID);
				if (contactMeMessageToDelete != null) {
					int recipientUserID = recipientUser.getId();
					if (contactMeMessageToDelete.getRecipientID() == recipientUserID) {
						contactMeMessageToDelete.setIsDeletedByRecipient(true);
						ContactMeMessage updatedMessage = contactMeMessageService.updateContactMeMessage(contactMeMessageToDelete);
						LOG.info(String.format("ContactMeController::deleteContactMeMessageForRecipient::Success::Deleted message for recepient succesfully requestMessage[%s]", updatedMessage.toString()));
						responseStatusCode = HttpStatus.OK;
					} else {
						LOG.error("ContactMeController::deleteContactMeMessageForRecipient::Security Error::Tried to delete message of another recipient.");
						responseStatusCode = HttpStatus.UNAUTHORIZED;
					}
				} else {
					LOG.warn("ContactMeController::deleteContactMeMessageForRecipient::Not Found::Tried to delete a non existing Contact Me Message.");
					responseStatusCode = HttpStatus.NOT_FOUND;
				}
			} else {
				LOG.error("ContactMeController::deleteContactMeMessageForRecipient::Security Error::Tried to delete message with non existing user session.");
				responseStatusCode = HttpStatus.UNAUTHORIZED;
			}
		} else {
			LOG.info("ContactMeController::deleteContactMeMessageForRecipient::Security Error::Tried to delete message without authenticated token.");
			responseStatusCode = HttpStatus.UNAUTHORIZED;
		}

		return new ResponseEntity(responseStatusCode);
	}

	@GetMapping(value = "api/contactMe", produces = "application/json")
	public ResponseEntity<List<ContactMeMessage>> getContactMeMessages() {
		LOG.info(String.format("Start::ContactMeController::getContactMeMessages"));
		HttpStatus responseStatusCode = HttpStatus.INTERNAL_SERVER_ERROR;
		List<ContactMeMessage> allUserMessages = new ArrayList<ContactMeMessage>();
		String messageForResponse = null;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		LOG.info(String.format("ContactMeController::HandleContactMeRequest::auth[%s]::isAuthenticated[%s]",
				auth.getName(), String.valueOf(auth.isAuthenticated())));

		if (auth != null && auth.isAuthenticated()) {
			User user = userService.getUser(auth.getName());
			if (user != null) {
				allUserMessages = contactMeMessageService.getUserContactMeMessages(user);
				responseStatusCode = HttpStatus.OK;
			} else {
				responseStatusCode = HttpStatus.NOT_FOUND;
			}
		} else {
			responseStatusCode = HttpStatus.UNAUTHORIZED;
		}

		ResponseEntity res;
		if (responseStatusCode == HttpStatus.OK) {
			res = new ResponseEntity<List<ContactMeMessage>>(allUserMessages, responseStatusCode);
		} else {
			res = new ResponseEntity<String>(messageForResponse, responseStatusCode);
		}
		LOG.info(String.format("End::getContactMeMessages::response[%s]", LoggingUtils.DisplayHTTPResponseData(res)));
		return res;
	}

}
