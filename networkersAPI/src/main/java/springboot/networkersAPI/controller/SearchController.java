package springboot.networkersAPI.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.BooleanExpression;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import springboot.networkersAPI.model.QUser;
import springboot.networkersAPI.model.RequestUserSearch;
import springboot.networkersAPI.model.User;
import springboot.networkersAPI.model.UserDTO;
import springboot.networkersAPI.model.eArea;
import springboot.networkersAPI.model.eDegreeType;
import springboot.networkersAPI.model.eOccupation;
import springboot.networkersAPI.service.UserService;
import springboot.networkersAPI.utils.CommonBeansContainer;
import springboot.networkersAPI.utils.LoggingUtils;

@RestController
public class SearchController {

	private final static Logger LOG = Logger.getLogger(SearchController.class);
	private final static String mAiSearchApiRoute = "http://127.0.0.1:5000/search";

	@Autowired
	private UserService userService;

	@Autowired
	private CommonBeansContainer commonBeansContainer;

	SearchController() {

	}

	@PostMapping(value = "api/search", produces = "application/json")
	public ResponseEntity<Set<UserDTO>> search(@RequestBody RequestUserSearch searchParamsMap) {
		LOG.info("Start::SearchController::search");
		LOG.info(String.format("SearchController::search::Original query:: %s", searchParamsMap));
		Set<UserDTO> searchResults = new HashSet<UserDTO>();
		Set<UserDTO> aiSearchResults = new HashSet<UserDTO>();
		HttpStatus responseStatusCode;
		try {
			if (searchParamsMap.getAreaStr() != null || searchParamsMap.getDegreeStr() != null
					|| searchParamsMap.getOccupationStr() != null || searchParamsMap.getRequestedSalary() != null
					|| searchParamsMap.getYearsOfExperience() != null) {
				List<String> skills = searchParamsMap.getSkills();
				searchParamsMap.setSkills(null);
				List<RequestUserSearch> queriesFromAi = getAiQueries(searchParamsMap);
				searchParamsMap.setSkills(skills);
				if (queriesFromAi != null && !queriesFromAi.isEmpty()) {
					LOG.info(String.format("SearchController::search::AI queries:: %s", queriesFromAi));
					for (RequestUserSearch aiQuery : queriesFromAi) {
						Iterable<User> curAiQueryResults = buildAndExecuteQuery(aiQuery);
						if (curAiQueryResults != null) {
							int i = 0;
							for (User user : curAiQueryResults) {
								if (i++ > 20) // maximum 20 users per AI query added to results
									break;
								UserDTO data = commonBeansContainer.getModelMapper().map(user, UserDTO.class);
								data.setIsFromAi(true);
								data.setIsFromOriginalSearch(false);
								aiSearchResults.add(data);
								searchResults.add(data);
							}
						}
					}
				}
				LOG.info(String.format("SearchController::search::%s matches found by AI queries",
						aiSearchResults.size()));
			}
			Iterable<User> allUsers = buildAndExecuteQuery(searchParamsMap);
			if (allUsers != null) {
				int i = 0;
				for (User user : allUsers) {
					if (i++ > 50)
						break; // allow only 50 results to be sent to client per query.
					UserDTO data = commonBeansContainer.getModelMapper().map(user, UserDTO.class);
					if (aiSearchResults.contains(data)) {
						data.setIsFromAi(true);
						searchResults.remove(data);
					} else {
						data.setIsFromAi(false);
					}
					data.setIsFromOriginalSearch(true);
					searchResults.add(data);
				}
			}
			responseStatusCode = HttpStatus.OK;

		} catch (Exception e) {
			responseStatusCode = HttpStatus.INTERNAL_SERVER_ERROR;
			LOG.error(String.format("Error::SearchController::search::Exception::%s", e.getCause()));
			e.printStackTrace();
		}
		ResponseEntity<Set<UserDTO>> response = new ResponseEntity<Set<UserDTO>>(searchResults, responseStatusCode);
		LOG.info("SearchController::search::result[" + searchResults.size() + "]");
		LOG.info(String.format("End::SearchController::search::response[%s]",
				LoggingUtils.DisplayHTTPResponseData(response))); // maybe this is too heavy
		return response;
	}

	private List<RequestUserSearch> getAiQueries(RequestUserSearch queryParamsFromUser) {
		HttpEntity<RequestUserSearch> query = new HttpEntity<RequestUserSearch>(queryParamsFromUser);
		List<RequestUserSearch> aiQueries = null;
		try {
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<List<RequestUserSearch>> response = restTemplate.exchange(mAiSearchApiRoute, HttpMethod.POST,
					query, new ParameterizedTypeReference<List<RequestUserSearch>>() {
					});
			HttpStatus statusCode = response.getStatusCode();
			LOG.info(String.format("SearchController::getAiQueries::response[%s]",
					LoggingUtils.DisplayHTTPResponseData(response)));
			if (statusCode.equals((HttpStatus.OK))) {
				aiQueries = response.getBody();
				LOG.info(
						String.format("SearchController::getAiQueries::Response status was OK - %s AI queries recieved",
								aiQueries.size()));
			} else {
				LOG.error(String
						.format("SearchController::getAiQueries::Response status was not OK - no AI queries recieved"));
			}
		} catch (Exception e) {
			LOG.error(String
			.format("SearchController::getAiQueries::Error occured while trying to get data from AI service - no AI queries recieved. error[%s]", e.getMessage()));
			e.printStackTrace();
		}

		return aiQueries;
	}

	private Iterable<User> buildAndExecuteQuery(RequestUserSearch userDataForSearch) {
		QUser quser = QUser.user;

		BooleanBuilder builder = new BooleanBuilder();

		// search only for active users
		BooleanExpression isActive = quser.active.eq(1);
		builder.and(isActive);

		// Add all search parameters here
		if (userDataForSearch.areaStr != null && !userDataForSearch.areaStr.isEmpty()) {
			eArea requestedArea = eArea.valueOf(userDataForSearch.areaStr);
			BooleanExpression isFromSearchedArea = quser.area.eq(requestedArea);
			builder.and(isFromSearchedArea);
		}

		if (userDataForSearch.degreeStr != null && !userDataForSearch.degreeStr.isEmpty()) {
			eDegreeType requestedDegree = eDegreeType.valueOf(userDataForSearch.degreeStr);
			BooleanExpression hasSearchedDegree = quser.degree.eq(requestedDegree);
			builder.and(hasSearchedDegree);
		}

		if (userDataForSearch.occupationStr != null && !userDataForSearch.occupationStr.isEmpty()) {
			eOccupation requestedOccupation = eOccupation.valueOf(userDataForSearch.occupationStr);
			BooleanExpression isFromSearchedOccupation = quser.occupation.eq(requestedOccupation);
			builder.and(isFromSearchedOccupation);
		}

		Integer requestedMaxSalary = userDataForSearch.getRequestedSalary();
		if (requestedMaxSalary != null) {
			try {
				BooleanExpression isRequestedSalaryLowerOrEqual = quser.requestedSalary.loe(requestedMaxSalary);
				builder.and(isRequestedSalaryLowerOrEqual);
			} catch (NumberFormatException e) {
			}
		}

		Integer requestedMinimalExperience = userDataForSearch.getYearsOfExperience();
		if (requestedMinimalExperience != null) {
			try {

				BooleanExpression isYearsOfExperienceGreaterOrEqual = quser.yearsOfExperience
						.goe(requestedMinimalExperience);
				builder.and(isYearsOfExperienceGreaterOrEqual);
			} catch (NumberFormatException e) {
			}
		}

		List<String> skills = userDataForSearch.getSkills();
		if (skills != null && !skills.isEmpty()) {
			for (String skill : skills) {
				BooleanExpression isFromSearchedSkill = quser.skills.containsIgnoreCase(skill);
				builder.and(isFromSearchedSkill);
			}
		}

		return userService.findAll(builder);
	}

}
