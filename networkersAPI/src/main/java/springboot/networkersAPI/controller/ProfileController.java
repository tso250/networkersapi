package springboot.networkersAPI.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import springboot.networkersAPI.model.User;
import springboot.networkersAPI.model.UserDTO;
import springboot.networkersAPI.service.UserService;
import springboot.networkersAPI.utils.CommonBeansContainer;
import springboot.networkersAPI.utils.LoggingUtils;
import springboot.networkersAPI.utils.eRequestMethod;

@RestController
public class ProfileController {

	private final static Logger LOG = Logger.getLogger(LoginController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private CommonBeansContainer commonBeansContainer;
	
	//private ModelMapper modelMapper = new ModelMapper(); // Maybe make this shared in whole program
	

	private Map<eRequestMethod, String> requestMethodToFunctionName;
	
	public ProfileController() {
	}
	
	@PostConstruct
	private void postConstruct() {
		requestMethodToFunctionName = new HashMap<eRequestMethod,String>();
		requestMethodToFunctionName.put(eRequestMethod.get, "getProfile");
		requestMethodToFunctionName.put(eRequestMethod.put, "updateProfile");
	}

	@GetMapping(value = "api/profile", produces = "application/json")
	public ResponseEntity getProfile() {
		LOG.info(String.format("Start::ProfileController::getProfile"));		
		return HandleProfileRequest(eRequestMethod.get ,null);
	}

	@PutMapping(value = "api/profile", produces = "application/json")
	public ResponseEntity updateProfile(@Valid @RequestBody UserDTO updatedUser) {
		LOG.info(String.format("Start::ProfileController::updateProfile"));	
		return HandleProfileRequest(eRequestMethod.put ,updatedUser);
	}

	private ResponseEntity HandleProfileRequest(eRequestMethod requestMethod, UserDTO updatedUser) {
		HttpStatus responseStatusCode;
		UserDTO userForResponse = null;
		String messageForResponse = null;
		String funtionNameForLogs = requestMethodToFunctionName.get(requestMethod);
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			LOG.info(String.format("ProfileController::%s::auth[%s]::isAuthenticated[%s]", funtionNameForLogs,
					auth.getName(), String.valueOf(auth.isAuthenticated())));
			if (auth != null && auth.isAuthenticated()) {
				User user = userService.getUser(auth.getName());
				if (user != null) {
					LOG.info(String.format("ProfileController::%s::user[%s]", funtionNameForLogs, user));
					if (requestMethod == eRequestMethod.get) {
						responseStatusCode = HttpStatus.OK;
					} else { // if(requestMethod == requestMethod.put) {
						responseStatusCode = HandleProfileUpdateRequest(user, updatedUser);
						user = userService.getUser(auth.getName());
					}
					userForResponse = commonBeansContainer.getModelMapper().map(user, UserDTO.class);
				} else {
					responseStatusCode = HttpStatus.NOT_FOUND;
					messageForResponse = "bad authentication no user with this email";
				}
			} else {
				responseStatusCode = HttpStatus.UNAUTHORIZED; // maybe change to bad_request
				messageForResponse = "bad authentication";
			}
		}
		catch(Exception e){
			responseStatusCode = HttpStatus.INTERNAL_SERVER_ERROR;
			messageForResponse = "Unknown server error";
			LOG.error(String.format("Error::ProfileController::%s::Exception::%s", funtionNameForLogs, e.getCause()));
			e.printStackTrace();
		}
		ResponseEntity res;
		if(responseStatusCode == HttpStatus.OK) {
			res = new ResponseEntity<UserDTO>(userForResponse, responseStatusCode);
		}
		else {
			res = new ResponseEntity<String>(messageForResponse, responseStatusCode);
		}
		LOG.info(String.format("End::ProfileController::%s::response[%s]", funtionNameForLogs, LoggingUtils.DisplayHTTPResponseData(res)));
		return res;
	}

	private HttpStatus HandleProfileUpdateRequest(User user, UserDTO updatedUser) {
		HttpStatus responseStatusCode;
		if (!user.getEmail().equalsIgnoreCase(updatedUser.getEmail())) {
			LOG.error(String.format("ProfileController::updateProfile::ClientSecurityIssue::user[%s]", user));
			//return res;
			responseStatusCode = HttpStatus.UNAUTHORIZED; // not sure about this
		}
		else {
			LOG.info(String.format("Start::ProfileController::updateProfile::user[%s]", user));

			// private info
			user.setFirstName(updatedUser.getFirstName());
			user.setLastName(updatedUser.getLastName());
			user.setPhoneNumber(updatedUser.getPhoneNumber());
			user.setDateOfBirth(updatedUser.getDateOfBirth());

			// enums
			user.setDegree(updatedUser.getDegree());
			user.setOccupation(updatedUser.getOccupation());
			user.setArea(updatedUser.getArea());
			user.setSkills(updatedUser.getSkills());

			// work info
			user.setWorkExperienceSummary(updatedUser.getWorkExperienceSummary());
			// TODO: price per hour
			// TODO: years of experience
			// TODO: is independent
			// TODO: linkedin
			// TODO: facebook

			user.setWasDataUpdated(true);
			responseStatusCode = userService.updateUser(user);
		}
		
		return responseStatusCode;	
	}
}
