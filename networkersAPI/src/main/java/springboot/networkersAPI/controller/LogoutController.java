package springboot.networkersAPI.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import springboot.networkersAPI.model.User;
import springboot.networkersAPI.service.UserService;
import springboot.networkersAPI.utils.LoggingUtils;

@RestController
public class LogoutController {

	private final static Logger LOG = Logger.getLogger(LoginController.class);

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	private UserService userService;

	public LogoutController() {
	}

	public static HttpSession getSession(boolean create) {
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = attributes.getRequest();
		return request.getSession(create);
	}

	@GetMapping(value = "api/logout", produces = "application/json")
	public ResponseEntity<String> logout() {
		//Response res = new Response();
		HttpStatus responseStatusCode;
		LOG.info(String.format("Start::LogoutController::logout"));
		try {
			SecurityContext sc = SecurityContextHolder.getContext();
			Authentication auth = sc.getAuthentication();
			User user = userService.getUser(auth.getName());
			if (user != null) {
				auth.setAuthenticated(false);
				HttpSession session = getSession(false);
				if (session != null) {
					session.invalidate();
				}
				SecurityContextHolder.clearContext();
				responseStatusCode = HttpStatus.OK;
				LOG.info(String.format("End::LogoutController::logout"));
			}
			else { // is this even possible ?
				responseStatusCode = HttpStatus.UNAUTHORIZED;
				LOG.info("LogoutController::logout::UNAUTHORIZED logout");
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error(String.format("Error::LogoutController::logout::Exception::%s", e.getCause()));
			responseStatusCode = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		ResponseEntity<String> response = new ResponseEntity<String>(responseStatusCode);
		LOG.info(String.format("End::LogoutController::logout::response[%s]", LoggingUtils.DisplayHTTPResponseData(response)));
		return response;
	}

}
