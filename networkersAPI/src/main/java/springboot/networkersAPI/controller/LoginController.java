package springboot.networkersAPI.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import springboot.networkersAPI.model.User;
import springboot.networkersAPI.service.UserService;
import springboot.networkersAPI.utils.LoggingUtils;

@RestController
public class LoginController {

	private final static Logger LOG = Logger.getLogger(LoginController.class);

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	private UserService userService;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	public LoginController() {
	}

	@PostMapping(value = "api/login", produces = "application/json")
	public ResponseEntity login(@RequestBody User user) {
		LOG.info(String.format("Start::LoginController::login::user[%s]", user));
		//Response res = new Response();
		boolean loginSucceeded = false;
		HttpStatus responseStatusCode;
		try {
			User checkLogin = userService.getUser(user.getEmail());
			String clientRawPassword = user.getPassword();

			if (checkLogin != null) {
				String serverEncodedPassword = checkLogin.getPassword();
				if (passwordEncoder.matches(clientRawPassword, serverEncodedPassword)) {
					UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(
							user.getEmail(), user.getPassword());
					Authentication auth = authenticationManager.authenticate(authReq);
					SecurityContext sc = SecurityContextHolder.getContext();
					sc.setAuthentication(auth);
					// res.setIsStatusOK(true);
					loginSucceeded = true;
				}
			}

			if (!loginSucceeded) // (!res.getIsStatusOK())
			{
				responseStatusCode = HttpStatus.UNAUTHORIZED;
				// res.setMessage("bad credentials");
			} else {
				responseStatusCode = HttpStatus.OK;
			}
		}
		catch(Exception e) {
			LOG.error(String.format("Error::LoginController::login::Exception::%s", e.getCause()));
			e.printStackTrace();
			responseStatusCode = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		//LOG.info(String.format("End::LoginController::login::user[%s]::res[%s]", user, res));
		ResponseEntity response = new ResponseEntity(responseStatusCode);
		LOG.info(String.format("End::LoginController::login::user[%s]::response[%s]", user, LoggingUtils.DisplayHTTPResponseData(response)));

		return response;
		
	}

}
