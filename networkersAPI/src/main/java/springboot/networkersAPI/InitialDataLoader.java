package springboot.networkersAPI;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import springboot.networkersAPI.service.RoleService;

@Component
public class InitialDataLoader implements ApplicationRunner {

    
	private final static Logger LOG = Logger.getLogger(InitialDataLoader.class);
	@Autowired
	private RoleService roleService;


    public void run(ApplicationArguments args) {
    	LOG.info("Start::InitialDataLoader::LOG::run");
    	roleService.initRoles();
    	LOG.info("End::InitialDataLoader::LOG::run");
    }
}