package springboot.networkersAPI.model;

import java.util.Date;

public class UserDTO implements java.io.Serializable{

	private String email;

	private String firstName;

	private String lastName;

	private String phoneNumber;

	private Date dateOfBirth;

	private eDegreeType degree;

	private eOccupation occupation;

	private eArea area;

	private int id;

	private int yearsOfExperience;

	private int requestedSalary;

	private String workExperienceSummary;

	private Boolean independent;

	private String FacebookPageUrl;

	private String LinkedinPageUrl;

	private int active;

	private String skills;
	
	private Boolean isFromAi;
	
	private Boolean isFromOriginalSearch;

	public UserDTO() {
	}

	public String getSkills() {
		return skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		String firstNameUpperCase = firstName.substring(0, 1).toUpperCase() + firstName.substring(1);
		return firstNameUpperCase;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		String firstNameUpperCase = firstName.substring(0, 1).toUpperCase() + firstName.substring(1);
		this.firstName = firstNameUpperCase;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		String lastNameUpperCase = lastName.substring(0, 1).toUpperCase() + lastName.substring(1);
		return lastNameUpperCase;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		String lastNameUpperCase = lastName.substring(0, 1).toUpperCase() + lastName.substring(1);
		this.lastName = lastNameUpperCase;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the degree
	 */
	public eDegreeType getDegree() {
		return degree;
	}

	/**
	 * @param degree the degree to set
	 */
	public void setDegree(eDegreeType degree) {
		this.degree = degree;
	}

	/**
	 * @return the occupation
	 */
	public eOccupation getOccupation() {
		return occupation;
	}

	/**
	 * @param occupation the occupation to set
	 */
	public void setOccupation(eOccupation occupation) {
		this.occupation = occupation;
	}

	/**
	 * @return the area
	 */
	public eArea getArea() {
		return area;
	}

	/**
	 * @param area the area to set
	 */
	public void setArea(eArea area) {
		this.area = area;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the yearsOfExperience
	 */
	public int getYearsOfExperience() {
		return yearsOfExperience;
	}

	/**
	 * @param yearsOfExperience the yearsOfExperience to set
	 */
	public void setYearsOfExperience(int yearsOfExperience) {
		this.yearsOfExperience = yearsOfExperience;
	}

	/**
	 * @return the requestedSalary
	 */
	public int getRequestedSalary() {
		return requestedSalary;
	}

	/**
	 * @param requestedSalary the requestedSalary to set
	 */
	public void setRequestedSalary(int requestedSalary) {
		this.requestedSalary = requestedSalary;
	}

	/**
	 * @return the workExperienceSummary
	 */
	public String getWorkExperienceSummary() {
		return workExperienceSummary;
	}

	/**
	 * @param workExperienceSummary the workExperienceSummary to set
	 */
	public void setWorkExperienceSummary(String workExperienceSummary) {
		this.workExperienceSummary = workExperienceSummary;
	}

	/**
	 * @return the independent
	 */
	public Boolean getIndependent() {
		return independent;
	}

	/**
	 * @param independent the independent to set
	 */
	public void setIndependent(Boolean independent) {
		this.independent = independent;
	}

	/**
	 * @return the facebookPageUrl
	 */
	public String getFacebookPageUrl() {
		return FacebookPageUrl;
	}

	/**
	 * @param facebookPageUrl the facebookPageUrl to set
	 */
	public void setFacebookPageUrl(String facebookPageUrl) {
		FacebookPageUrl = facebookPageUrl;
	}

	/**
	 * @return the linkedinPageUrl
	 */
	public String getLinkedinPageUrl() {
		return LinkedinPageUrl;
	}

	/**
	 * @param linkedinPageUrl the linkedinPageUrl to set
	 */
	public void setLinkedinPageUrl(String linkedinPageUrl) {
		LinkedinPageUrl = linkedinPageUrl;
	}

	/**
	 * @return the active
	 */
	public int getActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(int active) {
		this.active = active;
	}

	public Boolean getIsFromAi() {
		return isFromAi;
	}

	public void setIsFromAi(Boolean isFromAi) {
		this.isFromAi = isFromAi;
	}

	public Boolean getIsFromOriginalSearch() {
		return isFromOriginalSearch;
	}

	public void setIsFromOriginalSearch(Boolean isFromOriginalSearch) {
		this.isFromOriginalSearch = isFromOriginalSearch;
	}

	@Override
	public int hashCode() {
		return id;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserDTO other = (UserDTO) obj;
		if (id != other.id)
			return false;
	
		return true;
	}


	
	
}