package springboot.networkersAPI.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "user")
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "linkedin")
	private String linkedinPageUrl;

	@Column(name = "facebook")
	private String facebookPageUrl;

	@Column(name = "password")
	@Length(min = 6, message = "*Your password must have at least 6 characters")
	@NotEmpty(message = "*Please provide your password")
	private String password;

	@Column(name = "email", unique = true)
	@Email(message = "*Please provide a valid Email")
	@NotEmpty(message = "*Please provide an email")
	private String email;

	@Column(name = "firstName")
	private String firstName;

	@Column(name = "lastName")
	private String lastName;

	@Column(name = "phoneNumber")
	private String phoneNumber;

	@Column(name = "dateOfBirth")
	@Temporal(TemporalType.DATE)
	private Date dateOfBirth;

	@Enumerated(EnumType.STRING)
	@Column(name = "degree")
	private eDegreeType degree;

	@Enumerated(EnumType.STRING)
	@Column(name = "occupation")
	private eOccupation occupation;

	@Enumerated(EnumType.STRING)
	@Column(name = "area")
	private eArea area;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id", unique = true)
	private int id;

	@Column(name = "yearsOfExperience")
	private int yearsOfExperience;

	@Column(name = "requestedSalary")
	private int requestedSalary;

	@Column(name = "workExperienceSummary")
	private String workExperienceSummary;

	@Column(name = "skills")
	private String skills;

	@Column(name = "independent")
	private Boolean independent;

	@Column(name = "active")
	private int active;
	
	@Column(name = "wasDataUpdated")
	private Boolean wasDataUpdated;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public Boolean getWasDataUpdated() {
		return wasDataUpdated;
	}

	public void setWasDataUpdated(Boolean wasDataUpdated) {
		this.wasDataUpdated = wasDataUpdated;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	/**
	 * @return the linkedinPageUrl
	 */
	public String getLinkedinPageUrl() {
		return linkedinPageUrl;
	}

	/**
	 * @param userName the linkedinPageUrl to set
	 */
	public void setLinkedinPageUrl(String linkedinPageUrl) {
		this.linkedinPageUrl = linkedinPageUrl;
	}

	/**
	 * @return the facebookPageUrl
	 */
	public String getFacebookPageUrl() {
		return facebookPageUrl;
	}

	/**
	 * @param facebookPageUrl the facebookPageUrl to set
	 */
	public void setFacebookPageUrl(String facebookPageUrl) {
		this.facebookPageUrl = facebookPageUrl;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		String firstNameUpperCase = firstName.substring(0, 1).toUpperCase() + firstName.substring(1);
		return firstNameUpperCase;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		String firstNameUpperCase = firstName.substring(0, 1).toUpperCase() + firstName.substring(1);
		this.firstName = firstNameUpperCase;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		String lastNameUpperCase = lastName.substring(0, 1).toUpperCase() + lastName.substring(1);
		return lastNameUpperCase;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		String lastNameUpperCase = lastName.substring(0, 1).toUpperCase() + lastName.substring(1);
		this.lastName = lastNameUpperCase;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the degree
	 */
	public eDegreeType getDegree() {
		return degree;
	}

	/**
	 * @param degree the degree to set
	 */
	public void setDegree(eDegreeType degree) {
		this.degree = degree;
	}

	/**
	 * @return the occupation
	 */
	public eOccupation getOccupation() {
		return occupation;
	}

	/**
	 * @param occupation the occupation to set
	 */
	public void setOccupation(eOccupation occupation) {
		this.occupation = occupation;
	}

	/**
	 * @return the area
	 */
	public eArea getArea() {
		return area;
	}

	/**
	 * @param area the area to set
	 */
	public void setArea(eArea area) {
		this.area = area;
	}

	/**
	 * @return the yearsOfExperience
	 */
	public int getYearsOfExperience() {
		return yearsOfExperience;
	}

	/**
	 * @param yearsOfExperience the yearsOfExperience to set
	 */
	public void setYearsOfExperience(int yearsOfExperience) {
		this.yearsOfExperience = yearsOfExperience;
	}

	/**
	 * @return the requestedSalary
	 */
	public int getRequestedSalary() {
		return requestedSalary;
	}

	/**
	 * @param requestedSalary the requestedSalary to set
	 */
	public void setRequestedSalary(int requestedSalary) {
		this.requestedSalary = requestedSalary;
	}

	/**
	 * @return the workExperienceSummary
	 */
	public String getWorkExperienceSummary() {
		return workExperienceSummary;
	}

	/**
	 * @param workExperienceSummary the workExperienceSummary to set
	 */
	public void setWorkExperienceSummary(String workExperienceSummary) {
		this.workExperienceSummary = workExperienceSummary;
	}

	/**
	 * @return the skills
	 */
	public String getSkills() {
		return skills;
	}

	/**
	 * @param skills the skills to set
	 */
	public void setSkills(String skills) {
		this.skills = skills;
	}


	/**
	 * @return the independent
	 */
	public Boolean getIndependent() {
		return independent;
	}

	/**
	 * @param independent the independent to set
	 */
	public void setIndependent(Boolean independent) {
		this.independent = independent;
	}

	@Override
	public String toString() {
		return "User [email=" + email + "]";
	}

}
