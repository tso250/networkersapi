package springboot.networkersAPI.model;



public class MessageToSend {

	private int recipientID;
	
	private String messageBody;

	public MessageToSend() {
	}

	public int getRecipientID() {
		return recipientID;
	}

	public void setRecipientID(int recipientID) {
		this.recipientID = recipientID;
	}

	public String getMessageBody() {
		return messageBody;
	}

	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}
	
}
