package springboot.networkersAPI.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
@Entity
@Table(name = "messages")
public class ContactMeMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "messageID", unique = true)
	private Long messageID;
	
	@Column(name = "senderID")
	private int senderID;

	@Column(name = "senderDisplayName")
	private String senderDisplayName;
	
	@Column(name = "recipientID")
	private int recipientID;

	@Column(name = "messageBody")
	private String messageBody;

	@Column(name = "title")
	private String title;
	
	@Column(name = "timeSent")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar timeSent;

	@Column(name = "isReadByRecipient")
	private Boolean isReadByRecipient;

	@Column(name = "isDeletedByRecipient")
	private Boolean isDeletedByRecipient;

	public ContactMeMessage(){
		this.isReadByRecipient = false;
		this.isDeletedByRecipient = false;
	}

	public ContactMeMessage(int senderID, String senderDisplayName, int recipientID, String messageBody, Calendar timeSent, String title) {
		this.senderID = senderID;
		this.senderDisplayName = senderDisplayName;
		this.recipientID = recipientID;
		this.messageBody = messageBody;
		this.timeSent = timeSent;
		this.isReadByRecipient = false;
		this.isDeletedByRecipient = false;
		this.title = title;
	}

	public Long getMessageID() {
		return messageID;
	}

	public void setMessageID(Long messageID) {
		this.messageID = messageID;
	}

	public int getSenderID() {
		return senderID;
	}

	public void setSenderID(int senderID) {
		this.senderID = senderID;
	}

	public String getSenderDisplayName() {
		return senderDisplayName;
	}

	public void setSenderDisplayName(String senderDisplayName) {
		this.senderDisplayName = senderDisplayName;
	}

	public int getRecipientID() {
		return recipientID;
	}

	public void setRecipientID(int recipientID) {
		this.recipientID = recipientID;
	}

	public String getMessageBody() {
		return messageBody;
	}

	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Calendar getTimeSent() {
		return timeSent;
	}

	public void setTimeSent(Calendar timeSent) {
		this.timeSent = timeSent;
	}

	public Boolean getIsReadByRecipient() {
		return isReadByRecipient;
	}

	public void setIsReadByRecipient(Boolean isReadByRecipient) {
		this.isReadByRecipient = isReadByRecipient;
	}

	public Boolean getIsDeletedByRecipient() {
		return isDeletedByRecipient;
	}

	public void setIsDeletedByRecipient(Boolean isDeletedByRecipient) {
		this.isDeletedByRecipient = isDeletedByRecipient;
	}

}
