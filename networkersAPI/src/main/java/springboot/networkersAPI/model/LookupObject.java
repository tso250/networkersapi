package springboot.networkersAPI.model;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LookupObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String label;

	private String value;

	public LookupObject() {

	}

	public LookupObject(String value, String label) {
		this.label = label;
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}



}
