package springboot.networkersAPI.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;


@Data
@Builder
@AllArgsConstructor
public class Lookup implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final List<String> SupportedSkills = Collections.unmodifiableList(
		Arrays.asList(
		"Fire Safety", "Electrical Safety", "Building and Structural Safety", "Environmental Safety", "Workplace Safety", "General Safety", "Occupational Safety and Health", "Process and Production Safety",
		"People Skills", "Proper Writing Skills", "Business Knowledge", "Safety Conversation Skills", "Proper Record Keeping",
		"Legal Knowledge","Math","Communication","Problem Solving","Visualization",
		"Color Basics", "Spatial Awareness", "Harmony and Balance", "Budget Management", "Timeline Management", "Organization", "Technology",
		"Java","PHP","NodeJS","ReactJS","Angular","Python","Machine Learning","SQL","Windows","Linux","OOP","Design Patterns","C#","Scala","Ruby","Jenkins","CPP","VueJS","Mongo","Databases","NLP","Assembly", "Data Minning",
		"C++", "C Language",
		"Leadership", "Communication", "Scheduling", "Negotiating", "Cost Management", "Risk Management", "Time Management", "Team management", "Planning",
		"ARM","AVR","PIC","Circuit designing","Mathematics","Physics","MATLAB","Arduino"
		));

	public static List<LookupObject> AreasMap = Collections.unmodifiableList(
		Arrays.asList(
		new LookupObject(eArea.Center.name(), "Center"),
		new LookupObject(eArea.Israel.name(), "Israel"),
		new LookupObject(eArea.Jerusalem.name(), "Jerusalem"),
		new LookupObject(eArea.North.name(), "North"),
		new LookupObject(eArea.South.name(), "South")
		));

	public static List<LookupObject> DegreesMap = Collections.unmodifiableList(
		Arrays.asList(
		new LookupObject(eDegreeType.Engineer.name(), "Engineer"),
		new LookupObject(eDegreeType.Other.name(), "Other"),
		new LookupObject(eDegreeType.PracticalEngineer.name(), "Practical Engineer"),
		new LookupObject(eDegreeType.Technician.name(), "Technician")
		));

	public static List<LookupObject> OccupationsMap = Collections.unmodifiableList(
		Arrays.asList(
		new LookupObject(eOccupation.Architecture.name(), "Architecture"),
		new LookupObject(eOccupation.IndustrialElectricity.name(), "Industrial Electricity"),
		new LookupObject(eOccupation.InteriorDesign.name(), "Interior Design"),
		new LookupObject(eOccupation.ProjectManagement.name(), "Project Management"),
		new LookupObject(eOccupation.Safety.name(), "Safety"),
		new LookupObject(eOccupation.SoftwareEngineer.name(), "Software Engineer"),
		new LookupObject(eOccupation.QualityEngineer.name(), "Quality Engineer")
		));


	private List<String> skills;

	private List<LookupObject> areas;

	private List<LookupObject> degrees;

	private List<LookupObject> occupations;

	public Lookup() {
		this.setSkills(SupportedSkills);
		this.setAreas(AreasMap);
		this.setDegrees(DegreesMap);
		this.setOccupations(OccupationsMap);
	}

	public List<LookupObject> getOccupations() {
		return occupations;
	}

	public void setOccupations(List<LookupObject> occupations) {
		this.occupations = occupations;
	}

	public List<LookupObject> getDegrees() {
		return degrees;
	}

	public void setDegrees(List<LookupObject> degrees) {
		this.degrees = degrees;
	}

	public List<LookupObject> getAreas() {
		return areas;
	}

	public void setAreas(List<LookupObject> areas) {
		this.areas = areas;
	}

	public List<String> getSkills() {
		return skills;
	}

	public void setSkills(List<String> skills) {
		this.skills = skills;
	}

}
