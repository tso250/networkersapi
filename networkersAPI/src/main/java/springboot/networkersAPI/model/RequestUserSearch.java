package springboot.networkersAPI.model;

import java.util.List;

public class RequestUserSearch {

	public RequestUserSearch(){

	}

	public String areaStr;

	public String degreeStr;

	public String occupationStr;

	private Integer requestedSalary;

	private Integer yearsOfExperience;

	private List<String> skills;

	/**
	 * @return the areaStr
	 */
	public String getAreaStr() {
		return areaStr;
	}

	/**
	 * @param areaStr the areaStr to set
	 */
	public void setAreaStr(String areaStr) {
		this.areaStr = areaStr;
	}

	/**
	 * @return the degreeStr
	 */
	public String getDegreeStr() {
		return degreeStr;
	}

	/**
	 * @param degreeStr the degreeStr to set
	 */
	public void setDegreeStr(String degreeStr) {
		this.degreeStr = degreeStr;
	}

	/**
	 * @return the occupationStr
	 */
	public String getOccupationStr() {
		return occupationStr;
	}

	/**
	 * @param occupationStr the occupationStr to set
	 */
	public void setOccupationStr(String occupationStr) {
		this.occupationStr = occupationStr;
	}

	public Integer getRequestedSalary() {
		return requestedSalary;
	}

	public void setRequestedSalary(Integer requestedSalary) {
		this.requestedSalary = requestedSalary;
	}

	
	public Integer getYearsOfExperience() {
		return yearsOfExperience;
	}

	public void setYearsOfExperience(Integer yearsOfExperience) {
		this.yearsOfExperience = yearsOfExperience;
	}

	public List<String> getSkills() {
		return skills;
	}

	public void setSkills(List<String> skills) {
		this.skills = skills;
	}

	@Override
	public String toString() {
		return "RequestUserSearch [areaStr=" + areaStr + ", degreeStr=" + degreeStr + ", occupationStr=" + occupationStr
				+ ", requestedSalary=" + requestedSalary + ", yearsOfExperience=" + yearsOfExperience + "]";
	}
}
