package springboot.networkersAPI.model;

public enum eOccupation {
	Safety,
	ProjectManagement,
	IndustrialElectricity,
	Architecture,
	InteriorDesign,
	SoftwareEngineer,
	QualityEngineer
}
