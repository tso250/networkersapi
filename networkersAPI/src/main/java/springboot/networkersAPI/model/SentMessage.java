package springboot.networkersAPI.model;

public class SentMessage {
	
	private String messageBody;

	private String title;

	public SentMessage() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessageBody() {
		return messageBody;
	}

	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}
	
}
